import { LitElement, html, css } from 'lit-element';

class RmFooter extends LitElement {

  static get properties() {
    return {
    };
  }

  static get styles() {
    return css`
      h5{
        color: black;
        padding: 10px;
        text-align: center;
        font-size: 22px;
      }

    `;
  }

  constructor() {
    super();


  }


  render() {
    return html`
      <h5>Hackaton 2021 BBVA | Grupo 1</h5>
    `;
  }

}

customElements.define('rm-footer', RmFooter);
