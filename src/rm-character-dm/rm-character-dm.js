import { LitElement, html } from 'lit-element';

class RmCharacterDm extends LitElement{

  static get properties() {
    return {
      characters: {type: Array},
      filters: {type: Object},
      page: {type : Number},
      next: {type : String},
      prev: {type : String},
      requestedPage: {type : String}
    };
  }

  constructor(){
    super();

    this.characters = [];
    this.getCharactersData();
  }

  getCharactersData(requestedPage){
    console.log("getCharactersData");
    console.log("Obteniendo datos de los personajes");

    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if(xhr.status === 200){
        console.log("Peticion completada correctamente");

        let APIResponse = JSON.parse(xhr.responseText);

        console.log(APIResponse);
        this.characters = APIResponse.results;
       // this.pages = APIResponse.pages;
       this.next = APIResponse.info.next;
       this.prev = APIResponse.info.prev;

      }
    }
    
    let queryParams = this.filters ? this._getFilterURL(this.filters) : '';
    const endpoint = requestedPage ? requestedPage : `https://rickandmortyapi.com/api/character/${queryParams}`

    console.log(queryParams);

    xhr.open("GET", endpoint);
    xhr.send();
    console.log("Fin de getCharactersData");
  }

  _getFilterURL(filters) {
    return '?' + Object.entries(filters).map(keyValue => keyValue[1] ? keyValue[0]+'='+keyValue[1] : '').filter(Boolean).join('&');

  }

    updated(changedProperties){
    console.log("updated");

    if(changedProperties.has("characters")){
      console.log("Ha cambiado la propiedad characters en character dm");

      this.dispatchEvent(new CustomEvent("character-list-updated" , {
        detail:{
          characters: this.characters,
          next: this.next,
          prev: this.prev
        }
      }));
    }

    if(changedProperties.has("filters")) {
      console.log("Ha cambiado la propiedad filters en character-dm");
      console.log(this.filters);

      this.getCharactersData();

    }

    if (changedProperties.has("requestedPage")){
      this.getCharactersData(this.requestedPage);

    }
  }
}
customElements.define('rm-character-dm', RmCharacterDm)
