import { LitElement, html } from 'lit-element';
import '../rm-main/rm-main.js';
import '../rm-header/rm-header.js';
import '../rm-footer/rm-footer.js';
import '../rm-character-filters/rm-character-filters.js';

class RmApp extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor () {
        super();
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <rm-header></rm-header>
            <div class="row row-cols-1 row-cols-sm-1 row-cols-md-4 ">
              <div class="col-md-2">
                <rm-character-filters @filter-characters="${this.filterCharacters}"></rm-character-filters>
              </div>
              <div class="col-md-10">
                <rm-main></rm-main>
              </div>
            </div>
            <rm-footer></rm-footer>
        `;
    }

    filterCharacters(e) {
      console.log("filterCharacters");
      console.log(e.detail.filter);

      this.shadowRoot.querySelector("rm-main").filterCharacter = e.detail.filter;

    }

}

customElements.define('rm-app', RmApp)
