import { LitElement, html, css } from 'lit-element';

class RmCharacterFilters extends LitElement {

  static get properties() {
    return {

    };
  }

  static get styles() {
    return css`
      input{
        color: greenyellow;
      }
      input::placeholder{
        color: greenyellow;
      }
      option{
        color: greenyellow;
      }
      select{
        color: greenyellow;
      }
      button{
        background-color: greenyellow;
        color: black;
        border-radius: 10px;
      }
      .d-flex{
        margin-bottom: 10px;
      }

    `;
  }

  constructor() {
    super();

    this.peopleStats = {};


  }


  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
    <div class="d-flex flex-column">
      <label>Nombre</label>
      <input id="nameFilter" type="text" placeholder="Nombre" class="mt-1 bg-dark" ></input>
      <label class="mt-3">Estado</label>
      <select class="mt-1 bg-dark" id="statusFilter">
        <option value="">Todos</option>
        <option value="alive">Vivo</option>
        <option value="dead">Muerto</option>
        <option value="unknown">Desconocido</option>
      </select>
      <label class="mt-3">Género</label>
      <select class="mt-1 bg-dark" id="genderFilter">
        <option value="">Todos</option>
        <option value="male">Masculino</option>
        <option value="female">Femenino</option>
        <option value="genderless">Sin género</option>
        <option value="unknown">Desconocido</option>
      </select>
      <button @click="${this.filterCharacter}" class="mt-4 p-2"><strong>Filtrar</strong></button>
    </div>
    `;
  }

  filterCharacter(e) {
    const name = this.shadowRoot.getElementById("nameFilter").value;
    const status = this.shadowRoot.getElementById("statusFilter").value;
    const gender = this.shadowRoot.getElementById("genderFilter").value;

    this.dispatchEvent(new CustomEvent("filter-characters", {
      detail: {
        filter: {
          name,
          status,
          gender
        }
      }
    }));
  }

}

customElements.define('rm-character-filters', RmCharacterFilters);
