import { LitElement, html, css } from 'lit-element';

class RmHeader extends LitElement {

  static get properties() {
    return {
    };
  }

    static get styles() {
    return css`
      header{
        text-align: center;
        padding: 15px;
      }
      header img{
        width: 100%;
        max-width: 650px;
      }
    `;
  }

  constructor() {
    super();


  }


  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <header>
        <img src="../../images/logo_header.png" alt="">
      </header>
    `;
  }

}

customElements.define('rm-header', RmHeader);
