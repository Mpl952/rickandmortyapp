import { LitElement, html, css } from 'lit-element';

class RmCharacterDetail extends LitElement {

    static get properties() {
        return {
            id : {type: Number},
            character: {type: Object},
            lastEpisode: {type: Object}
        };
    }

    static get styles() {
    return css`
      .card{
        border-color: yellowgreen;
        border-radius: 15px;
        overflow: hidden;
        margin: auto;
        padding: 10px;
      }
      li{
          line-height: 1.5;
      }

      button{
        background-color: greenyellow;
        color: black;
        border-radius: 10px;
        padding: 15px;
        font-weight: 600;
      }

      img{
        width: 100%;
        max-width: 400px;
      }
      .image{
          text-align: center;
      }

    `;
  }

    constructor () {
        super();
        this.character = {};
        this.lastEpisode = {};
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div class="card col-10 bg-dark">
                <div class="row row-cols-1 row-cols-xl-2" >
                    <div class="image">
                        <img src="${this.character.image}" class="rounded-circle"/>
                    </div>
                    <div class="card-body d-flex flex-column align-items-start">
                        <h3>Character data:</h3>
                        <ul>
                            ${Object.keys(this.character)
                                .filter(key => !["id", "url", "image", "created", "type"].includes(key))
                                .map(key => {
                                    let value = key === "episode" ? this.character[key].length : this.character[key];
                                    let clave = key === "episode" ? 'episodes' : key;
                                    return html`
                                        <li >${this.capitalizeFirstLetter(clave)}: <strong>${value?.name ?? value}</strong></li>
                                    `
                                })
                            }
                        </ul>
                        <h3>Last episode:</h3>
                        <ul>
                            ${Object.keys(this.lastEpisode)
                                .filter(key => !["id", "characters", "created", "url"].includes(key))
                                .map(key => {
                                    let value = this.lastEpisode[key];
                                    let clave = key;
                                    return html`
                                        <li >${this.capitalizeFirstLetter(clave)}: <strong>${value?.name ?? value}</strong></li>
                                    `
                                })
                            }
                        </ul>
                    </div>
                </div>
                <button class="mt-3" @click="${this.exitDetail}">Volver</button>
            </div>
        `;
    }

    updated(changedProperties) {
        if (changedProperties.has('id')){
            this.getCharacterData();
        }
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    exitDetail() {
        this.character = {};
        this.lastEpisode = {};
        this.id = '';
        this.dispatchEvent(new CustomEvent("exit-detail", {}))
    }

    getCharacterData(){
        console.log("getCharactersData");
        console.log("Obteniendo datos de personaje", this.id);
    
        if (this.id) {
            let xhr = new XMLHttpRequest();
            xhr.onload = () => {
              if(xhr.status === 200){
                console.log("Peticion completada correctamente");
        
                let APIResponse = JSON.parse(xhr.responseText);
        
                console.log(APIResponse);
                this.character = APIResponse;
                const episodes = APIResponse.episode;
                this.getCharacterLastEpisode(episodes[episodes.length-1]);
              }
            }
        
            xhr.open("GET", `https://rickandmortyapi.com/api/character/${this.id}`);
            xhr.send();
        }
        console.log("Fin de getCharactersData");
    }


    getCharacterLastEpisode(lastEpisodeUrl){
        console.log("getCharacterLastEpisode");
        console.log("Obteniendo datos del último episodio donde aparece el personaje", this.id);
    
        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
          if(xhr.status === 200){
            console.log("Peticion a getCharacterLocation completada correctamente");
    
            let APIResponse = JSON.parse(xhr.responseText);
    
            console.log(APIResponse);
            this.lastEpisode = APIResponse;
          }
        }
    
        xhr.open("GET", lastEpisodeUrl);
        xhr.send();
        console.log("Fin de getCharacterLocation");
    }
    

}

customElements.define('rm-character-detail', RmCharacterDetail)
