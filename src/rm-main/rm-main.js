import { LitElement, html, css } from 'lit-element';
import '../rm-character-list/rm-character-list.js';
import '../rm-character-detail/rm-character-detail.js';
import '../rm-character-dm/rm-character-dm.js';

class RmMain extends LitElement {

    static get properties() {
        return {
            displayDetail : {type: Boolean},
            filterCharacter: {type: Object}
        };
    }

    static get styles() {
    return css`
      main{
        min-height: 60vh;
      }

    `;
  }

    constructor () {
        super();
        this.displayDetail = false;
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <main>
                <rm-character-list
                    class="${this.displayDetail ? 'd-none' : ''}"
                    @go-to-page="${this.goToPage}"
                    @show-character-details="${this.showCharacterDetail}"></rm-character-list>
                <rm-character-detail
                    class="${this.displayDetail ? '' : 'd-none'}"
                    @exit-detail="${this.exitDetail}"
                ></rm-character-detail>
                <rm-character-dm @character-list-updated="${this.characterListUpdated}"></rm-character-dm>
            </main>
        `;
    }

    updated(changedProperties) {
      console.log("updated en rm-main");
      if(changedProperties.has("filterCharacter")) {
        this.shadowRoot.querySelector("rm-character-dm").filters = this.filterCharacter;
        this.displayDetail = false;
      }
    }

    characterListUpdated(ev){
        const characterList = this.shadowRoot.querySelector("rm-character-list");
        console.log(ev.detail);
        characterList.characters = ev.detail.characters;
        characterList.next = ev.detail.next;
        characterList.prev = ev.detail.prev;

    }

    showCharacterDetail(ev){
        this.displayDetail = true;

        this.shadowRoot.querySelector("rm-character-detail").id = ev.detail.id;
    }

    exitDetail(ev){
        this.displayDetail = false;
    }

    goToPage(e){
        this.shadowRoot.querySelector("rm-character-dm").requestedPage = e.detail.requestedPage;
    }

}

customElements.define('rm-main', RmMain)
