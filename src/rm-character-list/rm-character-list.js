import { LitElement, html, css } from 'lit-element';
import '../rm-character-card/rm-character-card.js';

class RmCharacterList extends LitElement {

    static get properties() {
        return {
            characters : {type: Array},
            next : {type: String},
            prev : {type: String}
        };
    }

    static get styles() {
    return css`
      .btns{
        margin-top: 10px;
      }
      button{
        width: 40px;
        height: 40px;
        background-color: greenyellow;
        color: black;
        border-radius: 10px;
      }

    `;
  }

    constructor () {
        super();
        this.characters = [];
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 row-cols-xl-5">
              ${this.characters.map(
                character => html`
                <div class="mt-3 text-center">
                  <rm-character-card 
                  id="${character.id}"
                  name="${character.name}" 
                  status="${character.status}" 
                  image="${character.image}"
                  @click="${this.showDetails}"
                  >
                </rm-character-card>
              </div>`
              )}
            </div>
            <div class="btns d-flex justify-content-around">
              <div>
                <button class="${!this.prev ? 'd-none' : ''}" style="font-size: 20px"
                @click="${this.goPrev}"><strong>&lt;</strong></button>
              </div>
              <div>
                <button class="${!this.next ? 'd-none' : ''}" style="font-size: 20px"
                @click="${this.goNext}"><strong>&gt;</strong></button>
              </div>
            </div>

        `;
    }

    showDetails(ev){
      this.dispatchEvent(new CustomEvent("show-character-details", {
        detail:{
          id: ev.target.id
        }
      }))
    }

    goNext(e){
      this.dispatchEvent (new CustomEvent("go-to-page",{
        detail:{
          requestedPage: this.next
        }
      }))
    }

    goPrev(e){
      this.dispatchEvent (new CustomEvent("go-to-page",{
        detail:{
          requestedPage: this.prev
        }
      }))
    }
}

customElements.define('rm-character-list', RmCharacterList)
