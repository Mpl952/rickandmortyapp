import { LitElement, html, css } from 'lit-element';

class RmCharacterCard extends LitElement {

    static get properties() {
        return {
            name : {type: String},
            status: {type: String},
            image : {type: String}
        };
    }

    static get styles() {
    return css`
      .card{
        border-color: yellowgreen;
        border-radius: 15px;
        padding: 10px 10px 0 10px;
      }

    `;
  }


    constructor () {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div class="card bg-dark h-100 border-2 rounded-3">
              <img class="card-img-top" src="${this.image}" alt="${this.name}">
              <div class="card-body">
                  <h5 class="card-title">${this.name}</h5>
                  <p class="card-text">${this.status}</p>
              </div>
            </div>

        `;
    }

}

customElements.define('rm-character-card', RmCharacterCard)
